const Discord = require('discord.js');
const fs = require('fs')

const client = new Discord.Client();

const players = require('./players.json')
const speaking = require('./speaking.json')
const id = require('./config.json').token


client.login(id);

async function onReady(){
  console.log('ready')
  let voiceChannel = {}
  client.channels.cache.forEach((channel)=>{
    if(channel.type === 'voice' && channel.name === 'General'){
      voiceChannel = channel
    }
  })

  try{
    const connection = await voiceChannel.join()
    /*
    console.log('conectado a canal de voz')
    */
  }catch(e){
    console.error("error conectandose al canal de voz", e)
  }
  
  client.on('guildMemberSpeaking', (member,sp) =>{
    if(players.indexOf(member.nickname) !== -1  || players.indexOf(member.user.username) !== -1){
      console.log("S")
      //console.log(member.nickname)
      //console.log(member.user.username)
      var nick = member.nickname  || member.user.username
      console.log(nick)

      var speakingObj = speaking.map((actual) =>{
        if(actual.nick === nick){
          actual.png = sp.bitfield === 1 
                      ? `${nick}_hablando.png` 
                      : `${nick}_calladx.png`
        }
        return actual
      },{})

      fs.writeFileSync('speaking.json', JSON.stringify(speakingObj,false,4))
    }
  });

}

client.on('ready', onReady)
